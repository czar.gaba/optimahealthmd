<?php
/**
 * Enqueue scripts & styles
 * 
 */
add_action( 'wp_enqueue_scripts', function() {
	// Enqueue main css file
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', [] );

	// Enqueue frontpage scripts & styles 
	if ( is_front_page() ) :
		wp_enqueue_style( 'frontpage-tailwind-css', get_stylesheet_directory_uri() . '/assets/css/style.min.css', false, 'all' );

		wp_enqueue_style( 'frontpage-swiper-css', get_stylesheet_directory_uri() . '/assets/js/script.min.css', false, 'all' );

		wp_register_script( 'frontpage-js', get_stylesheet_directory_uri() . '/assets/js/script.min.js', ['jquery'], '1.0', true );

		wp_enqueue_script( 'frontpage-js' );

		wp_localize_script( 'frontpage-js', 'cw_vars', array(
			'cw_nonce' => wp_create_nonce( 'platform_security' ),
			'cw_ajax_url' => admin_url( 'admin-ajax.php' )
		) );
	endif;

}, 20 );

/**
 * Load language files from Avada
 */
add_action( 'after_setup_theme', function() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
} );

/**
 * Allow uploading of SVG files to media library
 * 
 */
add_filter( 'upload_mimes', function( $mimes = array() ) {
	$mimes['svg']  = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';
	
	return $mimes;
} );   

/**
 * Ajax Filter
 * @url https://wp-qa.com/wordpress-check_ajax_referer-gives-403-forbidden-when-using-inside-ajax-action
 */
function cw_filter_services() {
	if ( wp_verify_nonce( $_POST['security'], 'platform_security' ) ) {
		// var_dump($_POST['security']);
	}

	$catSlug = $_POST['category'];

	$loop = array(
		'post_type' => 'service',
		'posts_per_page' => -1,
	);

	if ( $catSlug ) {
		$loop['tax_query'] = array(
			array(
				'taxonomy' => 'service_type',
				'field' => 'slug',
				'terms' => $catSlug,
				'operator' => 'IN'
			)
		);
	}

	$ajaxposts = new WP_Query( $loop );

	$response = '';

	if ( $ajaxposts->have_posts() ) :
		while ( $ajaxposts->have_posts() ) : $ajaxposts->the_post();
			$response .= get_template_part( 'templates/_components/accordion' );
		endwhile;
	endif;

	echo $response;

	exit;
}

add_action( 'wp_ajax_cw_filter_services', 'cw_filter_services' );
add_action( 'wp_ajax_nopriv_cw_filter_services', 'cw_filter_services' );

/**
 * Add hook to before main container in Avada
 * 
 */
add_action( 'avada_before_main_container', function() { ?>
	<!-- Hero  -->
	<?php if( have_rows('hero') ): ?>
		<?php while( have_rows('hero') ): the_row(); 
			get_template_part( 'templates/home/hero' ); ?>
		<?php endwhile; ?>
	<?php endif; ?>
	<!-- end hero  -->
<?php } );

/**
 * Add hook to after main container in Avada
 * 
 */
add_action( 'avada_after_main_container', function() { ?>
	<!-- Insights Section -->
	<?php
		if ( have_rows( 'our_insights' ) ) : ?>
		<div class="before-footer cw-bg-[#fafafa]">
			<div class="cw-mx-auto cw-container cw-max-w-7xl cw-py-40">
				<?php while ( have_rows( 'our_insights' ) ) : the_row(); ?>
					<?php get_template_part( 'templates/home/insights' ); ?>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>
	<!-- End Insights Section -->
<?php }, 10 );

add_action( 'avada_after_main_container', function() { ?>
	<!-- Slider Section -->
	<?php if( have_rows('slider') ): ?>
		<?php while( have_rows('slider') ): the_row(); 
			get_template_part( '/templates/home/slider' );		
		?>
	<?php endwhile; ?>
	<?php endif; ?>
	<!-- End Slider Section -->
<?php }, 20 );

add_action( 'avada_after_main_container', function() { ?>
	<!-- Bottom CTA Section -->
	<?php if( have_rows('cta') ): ?>
		<div class="bottom-cta cw-py-[120px] cw-bg-[#0062FD]">
			<div class="cw-mx-auto cw-container cw-max-w-7xl">
				<?php while( have_rows('cta') ): the_row(); ?>
					<?php get_template_part( 'templates/home/form' ); ?>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>
	<!-- End Bottom CTA Section -->
<?php }, 30 );