# Avada Child Theme

Avada child theme with Tailwind CSS workflow integrated.

## Building from source

1. Clone repository to your WordPress Theme directory.
2. Run `npm i -g browser-sync` to install `browser-sync` globally.
3. Run `npm install`.
4. Changed the proxy & host value in line `31` in `package.json` file.
5. Run `npm run build`.
