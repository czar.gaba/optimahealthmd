const $ = jQuery;

$(".filter-item-link").on("click", function (e) {
  e.preventDefault();

  $(".filter-item-link").removeClass("active");
  $(this).addClass("active");
  $url = $.ajax({
    type: "POST",
    url: cw_vars.cw_ajax_url,
    dataType: "html",
    data: {
      action: "cw_filter_services",
      category: $(this).data("slug"),
      security: cw_vars.cw_nonce,
    },
    beforeSend: function () {
      $(".services-accordion").addClass("loading");
    },
    success: function (res) {
      $(".services-accordion").html(res);
    },
    complete: function (res) {
      $(function () {
        $(".accordion-title").click(function (j) {
          var dropDown = $(this)
            .closest(".accordion-wrapper")
            .find(".accordion-content");
          $(this)
            .closest(".services-accordion")
            .find(".accordion-content")
            .not(dropDown)
            .slideUp();

          if ($(this).hasClass("active")) {
            $(this).removeClass("active");
          } else {
            $(this)
              .closest(".services-accordion")
              .find(".accordion-title.active")
              .removeClass("active");
            $(this).addClass("active");
          }

          dropDown.stop(false, true).slideToggle();
          j.preventDefault();
        });
      });
    },
  });
});

$(function () {
  $(".accordion-title").click(function (j) {
    var dropDown = $(this)
      .closest(".accordion-wrapper")
      .find(".accordion-content");
    $(this)
      .closest(".services-accordion")
      .find(".accordion-content")
      .not(dropDown)
      .slideUp();

    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
    } else {
      $(this)
        .closest(".services-accordion")
        .find(".accordion-title.active")
        .removeClass("active");
      $(this).addClass("active");
    }

    dropDown.stop(false, true).slideToggle();
    j.preventDefault();
  });
});
