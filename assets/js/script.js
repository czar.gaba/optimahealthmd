import Swiper, { Pagination } from "swiper";

import "swiper/css";
import "swiper/css/pagination";

const swiper = new Swiper(".swiper", {
  modules: [Pagination],
  slidesPerView: 1,
  pagination: {
    el: ".swiper-pagination",
  },
  autoplay: {
    delay: 1000,
  },
});
