<?php
/**
 * Template Name: Front Page Template
*/

get_header(); 

?>
<!-- Portal Section --> 
<?php if( have_rows('portal') ): ?>
	<div class="portal-section">
		<div class="cw-container cw-mx-auto cw-max-w-7xl">
			<?php while( have_rows('portal') ): the_row(); ?>
				<?php get_template_part( '/templates/home/links' ); ?>
			<?php endwhile; ?>
		</div>
	</div>
<?php endif; ?>
<!-- End Portal Section --> 

<!-- About Us Section --> 
<?php if( have_rows('about_us') ): ?>
	<div class="about-us cw-pt-32">
		<div class="cw-container cw-mx-auto cw-max-w-7xl">
			<?php while( have_rows('about_us') ): the_row(); ?>
				<?php get_template_part( '/templates/home/about' ); ?>
			<?php endwhile; ?>
		</div>
	</div>
<?php endif; ?>
<!-- End About Us Section -->

<!-- Why Choose Us -->
	<?php if ( have_rows( 'why_choose_us' ) ) :
		echo '<div class="why-choose-us cw-pt-32">';
			echo '<div class="cw-mx-auto cw-container cw-max-w-7xl">';
				while ( have_rows( 'why_choose_us' ) ) : the_row();
					get_template_part( 'templates/home/choose' );
				endwhile;
			echo '</div>';
		echo '</div>';
	endif; ?>
<!-- End Why Choose Us -->

<!-- Our Excellent Services -->
<?php
	if ( have_rows( 'our_services' ) ) :
		echo '<div class="cw-mx-auto our-services cw-pt-20 cw-pb-40">';
			echo '<div class="cw-mx-auto cw-container cw-max-w-7xl">';
				while ( have_rows( 'our_services' ) ) : the_row();
					get_template_part( 'templates/home/services' );
				endwhile;
			echo '</div>';
		echo '</div>';
	endif;
?>
<!-- End Our Excellent Services -->

<?php 
get_footer(); ?>