module.exports = {
  content: ["./*.php", "./templates/**/*.php"],
  prefix: "cw-",
  corePlugins: {
    preflight: false,
  },
  important: false,
  theme: {
    fontSize: {
      base: ["16px", "22px"],
    },
  },
  plugins: [],
};
