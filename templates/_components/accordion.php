<li class="accordion-wrapper cw-py-8 cw-border-t-0 cw-border-r-0 cw-border-l-0 cw-border-b cw-border-solid cw-border-b-[#E0E0E0]">
  <h4 class="accordion-title cw-relative cw-py-0 cw-cursor-pointer cw-px-0 cw-mx-0 cw-my-0"><?php echo get_the_title(); ?> <span class="cw-absolute cw-border-solid cw-block cw-w-[41px] cw-h-[41px] cw-border cw-border-[#e0e0e0] cw-rounded-full cw-right-0 cw-top-1/2 -cw-mt-5"></span></h4>
  <div class="accordion-content cw-py-8">
    <?php echo apply_filters( 'the_content', get_the_content() ); ?>
  </div>
</li>