<?php
  // Get sub field values.
  $title = get_sub_field('title');
  $content = get_sub_field('content');
  $link = get_sub_field('link');
  $image = get_sub_field( 'image' );
?>

<div class="side-image-wrapper cw-grid cw-grid-cols-2 cw-gap-16 cw-items-center">
  <div class="side-image cw-col-span-1 cw-relative">
    <?php if ( $image ) : ?>
      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="cw-object-fill cw-w-full" />
    <?php endif; ?>
  </div>
  <div class="side-image-content-wrapper cw-col-span-1">
      <div class="side-image-content"> 
        <div class="subheading"><?php echo $title; ?></div>
        <?php if ( $content ) : ?>
          <div class="content cw-text-[16px] cw-leading-[32px]">
            <?php echo wp_kses_post( $content ); ?>
          </div>  
        <?php endif; ?>
        <?php if ( $link ) : 
          echo '<div class="link-content cw-pt-10">';
            echo '<a class="cw-button cw-mx-auto" href="' . esc_url( $link ) . '">' . __( 'Read More', 'avada-child-theme' ) . '</a>';
          echo '</div>';
        endif; ?>
      </div>
  </div>
</div>