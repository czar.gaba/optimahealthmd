<?php
$slider = get_sub_field( 'slider' );
?>

<?php if ( have_rows( 'repeater' ) ) : ?>
  <div class="slider-section cw-py-36 cw-relative cw-bg-[#f5f5f5]">
    <div class="cw-container cw-mx-auto cw-max-w-7xl cw-relative">
      <div class="swiper cw-w-full">
        <div class="swiper-wrapper">
          <?php while ( have_rows( 'repeater' ) ) :  the_row(); ?>
            <?php 
              $image = get_sub_field( 'image' );
              $title = get_sub_field( 'title' );
              $content = get_sub_field( 'content' );
              $link = get_sub_field( 'link' );
            ?>
              <div class="swiper-slide cw-relative cw-min-h-[480px] cw-flex cw-items-end cw-py-24 cw-px-32">
                <div class="slider-image cw-absolute cw-left-0 cw-top-0 cw-w-full cw-h-full cw-z-10">
                  <img src="<?php echo esc_url( $image['url'] ); ?>" alt="<?php echo $title; ?>" class="cw-block cw-w-full cw-h-full cw-object-cover">
                </div>
                <div class="slider-content-wrapper cw-w-[600px] cw-relative cw-z-30">
                  <h3 class="slider-title cw-font-bold cw-text-white cw-leading-[58px] cw-text-[40px]"><?php echo $title; ?></h3>
                  <div class="slider-content cw-leading-[32px] cw-text-[16px] cw-text-white">
                    <?php echo wp_kses_post( $content ); ?>
                  </div>
                  <?php if ( $link ) : ?>
                    <a href="<?php echo esc_url( $link ); ?>" class="cw-button"><?php _e( 'Read More' ); ?></a>
                  <?php endif; ?>
                </div>
              </div> 
          <?php endwhile; ?>
        </div>
      </div>
      <div class="swiper-pagination !cw-text-left !-cw-bottom-8">&nbsp;</div>
    </div>
  </div>
<?php endif; ?>