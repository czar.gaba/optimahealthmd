<?php
  $title = get_sub_field('title');
  $content = get_sub_field('content');
  $link = get_sub_field('link'); 
?>

<div class="cw-grid cw-grid-cols-3 cw-grid-rows-2 cw-gap-10">
  <div class="cw-col-span-1 cw-row-span-2 heading-container">
    <?php if ( $title ) :
      echo '<h3 class="row-title cw-text-3xl cw-font-bold cw-pr-14 cw-mr-auto cw-my-0 cw-mb-12">' . $title . '</h3>';
    endif; ?>
    <?php if ( $link ) : 
      echo '<div class="heading-content cw-text-center">';
        echo '<a class="cw-button cw-mx-auto" href="' . esc_url( $link ) . '">' . __( 'Read All', 'avada-child-theme' ) . '</a>';
      echo '</div>';
    endif; ?>
  </div>
  
  <?php if ( have_rows( 'content' ) ) : ?>
    <?php $i = 0;
    while ( have_rows( 'content' ) ) : the_row();
      if ( $i % 2 == 0 ) {
        echo '<div class="cw-col-span-2 cw-row-span-1 cw-grid cw-grid-cols-2 cw-grid-row-2 cw-gap-10 cw-pt-12 cw-border-t cw-border-solid cw-border-l-0 cw-border-b-0 cw-border-r-0  cw-border-t-[#E0E0E0]">';
      }
      echo '<div class="cw-col-span-1 cw-row-span-1 cw-text-baseline cw-leading-8">';
        echo '<h4 class="cw-font-semibold cw-text-2xl cw-mb-5">' . get_sub_field( 'title' ) . '</h4>';
        echo wp_kses_post( get_sub_field( 'content' ) );
      echo '</div>';
      
      if ( $i % 2 != 0 ) {
        echo '</div>';
      }
      $i++;
    endwhile;
  endif; ?>
</div>