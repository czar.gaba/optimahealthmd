<?php
  $title = get_sub_field('title');
  $content = get_sub_field('content');
  $form_shortcode = get_sub_field('form_shortcode');
?>

<div class="form-wrapper cw-text-center cw-text-white">
  <h3 class="form-title cw-leading-[58px] cw-text-[40px] cw-font-bold cw-text-white cw-mx-0 cw-my-0"><?php echo $title; ?></h3>

  <div class="form-content cw-text-[24px] cw-leading-[38px]">
    <?php echo wp_kses_post( $content ); ?>

    <?php echo do_shortcode( $form_shortcode ); ?>
  </div>
</div>