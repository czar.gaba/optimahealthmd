<?php
// Get sub field values.
  $video = get_sub_field('video');
  $text = get_sub_field('text');
  $link = get_sub_field('link');
?>
<div class="hero cw-relative cw-overflow-hidden cw-text-white cw-flex cw-items-center cw-justify-center lg:cw-min-h-[792px]">
  <video autoplay muted loop id="myVideo" class="cw-absolute cw-z-10 cw-min-w-full cw-max-w-none cw-min-h-full">
    <source src="https://optima-client.cyberwrath.tech/wp-content/uploads/2022/09/Neurons-bg.mp4" type="video/mp4">
  </video>
	<div class="cw-mx-auto cw-container cw-max-w-7xl cw-relative cw-z-30">
		<div class="cw-grid cw-grid-cols-3 cw-gap-x-24 cw-items-center">
			<!-- text -->
			<div class="cta-text cw-col-span-1">
				<?php echo $text; ?>
        <div class="cta-text-button cw-mt-8">
				  <a href="<?php echo $link; ?>" class="cw-button btn-explore">Explore</a>
        </div>
			</div>
			<!-- oembed -->
			<div class="cta-video cw-col-span-2">
				<?php echo do_shortcode($video); ?>
			</div>
		</div>
	</div>

</div>