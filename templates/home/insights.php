<?php
  $title = get_sub_field('title');
  $content = get_sub_field('content');
  $link = get_sub_field('link'); 
?>

<div class="cw-flex cw-items-center heading-container cw-mb-12">
  <?php if ( $title ) :
    echo '<h3 class="row-title cw-text-3xl cw-font-bold cw-max-w-xs cw-mr-auto cw-my-0">' . $title . '</h3>';
  endif; ?>
  <?php if ( $link ) : 
    echo '<a class="cw-button-dark" href="' . esc_url( $link ) . '">' . __( 'Read All', 'avada-child-theme' ) . '<span class="arrow-right"></span></a>';
  endif; ?>
</div>

<?php if ( $content ) : ?>
  <div class="insight-posts cw-grid cw-grid-cols-none sm:cw-grid-cols-2 sm:cw-grid-rows-2 ">
    <?php $i = 0; ?>
    <?php foreach( $content as $insightpost ) : ?>
      <?php
        $classes = array(); 
        if ( $i == 0 ) :
          $classes[] = 'sm:cw-col-span-1 sm:cw-row-span-2 cw-relative';
        else:
          $classes[] = 'sm:cw-col-span-1 sm:cw-row-span-1 cw-relative';
        endif;

        $attributes = array(
          'class' => esc_attr( implode( ' ', $classes ) )
        );
      ?>
      <div <?php foreach( $attributes as $name => $value ) echo $name . '="' . $value . '" ' ?>>
        <div class="insight-post-image cw-w-full cw-relative cw-h-full">
          <?php echo get_the_post_thumbnail( $insightpost->ID, 'full', ['class' => '!cw-w-full !cw-h-full cw-object-cover' ] ); ?>
          <a class="cw-absolute cw-w-full cw-h-full cw-top-0 cw-left-0 cw-indent-[-9999px] cw-z-20" href="<?php echo esc_url( get_permalink( $insightpost->ID ) ); ?>">Read More</a>
        </div>
        <div class="insight-title cw-absolute cw-left-0 cw-bottom-0 cw-w-full cw-z-10 cw-pl-14 cw-pb-10">
          <h4 class="cw-max-w-md cw-font-medium <?php echo ( $i == 0 ) ? '!cw-text-[36px] !cw-leading-[45px]' : '!cw-text-[24px] !cw-leading-[35px]'; ?> cw-text-white cw-py-0 cw-mx-0"><?php echo get_the_title( $insightpost->ID ); ?></h4>
        </div>
      </div>
    <?php $i++; endforeach; ?>
  </div>
<?php endif; ?>