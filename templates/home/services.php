<?php

$title = get_sub_field('title');
$content = get_sub_field('content');
$link = get_sub_field('link'); ?>

<div class="cw-flex cw-flex-nowrap cw-items-baseline">

  <?php if ( $title ) : 
    echo '<h3 class="row-title cw-text-3xl cw-font-bold">' . $title . '</h3>'; 
  endif; ?>
  <?php 
    $args = array(
      'post_type' => 'service',
      'hide_empty' => false
    );

    $terms = get_terms( 'service_type', $args );
  ?>
  <div class="filter-navigation cw-text-right">
    <ul class="filter-lists cw-list-none cw-text-base cw-text-white cw-flex cw-flex-wrap cw-justify-end cw-space-x-4 cw-space-y-4">
      <li class="filter-item"><a class="filter-item-link active cw-no-underline" href="#!" data-type="">All</a></li>
      <?php foreach( $terms as $term ) : ?>
        <li class="filter-item"><a class="filter-item-link cw-no-underline" href="#!" data-slug="<?php echo $term->slug; ?>" data-type="service"><?php echo $term->name; ?></a></li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
<?php
  $services = new WP_Query( array(
    'post_type' => 'service',
    'posts_per_page' => -1,
  ) );

if ( $services->have_posts() ) :
  echo '<ul class="cw-list-none cw-p-0 cw-m-0 services-accordion">';
  while ( $services->have_posts() ) : $services->the_post(); ?>
    <?php get_template_part( 'templates/_components/accordion' ); ?>
  <?php endwhile;
  echo '</ul>';
  wp_reset_query();
endif; ?>

<?php if ( $link ) : 
  echo '<div class="heading-content cw-text-center cw-pt-12">';
    echo '<a class="cw-button cw-mx-auto" href="' . esc_url( $link ) . '">' . __( 'See all our services', 'avada-child-theme' ) . '</a>';
  echo '</div>';
endif; ?>