<?php
// Get sub field values.
  $title1 = get_sub_field('title1');
  $link1 = get_sub_field('link1');

  $title2 = get_sub_field('title2');
  $link2 = get_sub_field('link2');


  $title3 = get_sub_field('title3');
  $link3 = get_sub_field('link3');

  $links = array(
    array(
      'title' => $title1,
      'link' => $link1
    ),
    array(
      'title' => $title2,
      'link' => $link2
    ),
    array(
      'title' => $title3,
      'link' => $link3
    )
  );
?>

<?php if ( $links ) : ?>
  <ul class="portal-links cw-list-none cw-grid cw-items-center cw-grid-cols-3 cw-border-b cw-border-[#E0E0E0] cw-border-l-0 cw-border-r-0 cw-border-t-0 cw-border-solid cw-pb-7 cw-mb-0  cw-divide-x cw-divide-[#e0e0e0] cw-mx-0 cw-my-0 !cw-pl-0">
    <?php foreach( $links as $link ) { ?>
      <li class="portal-link">
        <a class="cw-text-[20px] cw-leading-[27px] cw-font-semibold cw-text-center cw-block cw-px-12 cw-py-6" href="<?php echo $link['link']; ?>"><?php echo $link['title']; ?><span class="arrow-right arrow-right-lg"></span></a>
      </li>
    <?php } ?>
<?php endif; ?>